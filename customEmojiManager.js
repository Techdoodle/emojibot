const EventEmitter = require('events');
const request = require('request');

class CustomEmojiManager extends EventEmitter
{
    constructor(client, emojiServers, customEmojiName, postUrl, postSecret)
    {
        super();
        
        this.emojis = [];
        this.customEmojiName = customEmojiName;
        
        // Assume the client is not ready yet
        client.on('ready', () =>
        {
            for(let guild of client.guilds.array())
            {
                // If this guild is an authorized custom emoji provider
                if(emojiServers.indexOf(guild.id) !== -1)
                {
                    // Add all the emoji
                    for(let emoji of guild.emojis.array())
                    {
                        this.addEmoji(emoji);
                    }
                }
            }
            
            let postEmoji = [];
            for(let emoji of this.emojis)
            {
                postEmoji.push({name: emoji.name, url: emoji.url});
            }
            // Upload them to the webserver
            
            request.post({
                url: postUrl,
                body: JSON.stringify({'secret': postSecret, 'emoji': postEmoji})
            }, (error, response, body) =>
            {
                if(error)
                {
                    console.log('Upload error: ' + error);
                }
                console.log('Uploaded!');
                console.log(body);
            });
            
            console.log('Added ' + this.emojis.length + ' custom emoji');
        });
    }
    
    getUniqueName(name, iteration)
    {
        // Create a unique name for the emoji
        let postFix = (iteration === 0) ? '' : '_' + iteration;
        
        for(let check of this.emojis)
        {
            if(check.name === name + postFix)
            {
                return this.getUniqueName(name, iteration + 1);
            }
        }
        return name + postFix;
    }
    
    addEmoji(emoji)
    {
        // Ensure there's no emoji with the same id
        for(let check of this.emojis)
        {
            if(check.id === emoji.id)
            {
                return;
            }
        }
        
        // Creates a clean copy of the data with a unique name
        let clean = {
            animated: emoji.animated,
            createdAt: emoji.createdAt,
            createdTimestamp: emoji.createdTimestamp,
            id: emoji.id,
            identifier: emoji.identifier,
            name_orig: emoji.name,
            name: this.getUniqueName(emoji.name, 0),
            url: emoji.url
        };
        
        if(clean.name !== this.customEmojiName)
        {
            this.emojis.push(clean);
            this.emit('emoji-add', clean);
        }
    }
    
    removeEmoji(id)
    {
        for(let emoji of this.emojis)
        {
            if(emoji.id === id)
            {
                this.emojis.splice(this.emojis.indexOf(emoji), 1);
                
                // Doesn't support duplicate ids
                break;
            }
        }
        this.emit('emoji-remove', id);
    }
    
    getEmoji(name)
    {
        for(let check of this.emojis)
        {
            if(check.name === name)
            {
                return check;
            }
        }
        return null;
    }
    
    //TODO handle emoji add, remove, and edit events
}

module.exports = CustomEmojiManager;