const Discord = require('discord.js');
const fs = require('fs');
const CustomEmojiManager = require('./customEmojiManager');

const client = new Discord.Client();

// Ugly but works
const settings = JSON.parse(fs.readFileSync('./settings.json'));

// List of emojis
let emojis = new CustomEmojiManager(client, settings['emoji-servers'], settings['custom-emoji-name'], settings['post-url'], settings['post-secret']);

// Map of user ids to the id of the message they're reacting to
let userWaiting = {};

// Array of bot reactions waiting to be seconded or timed out
let emojiWaiting = [];

function createMessage(title, text)
{
    let rich = new Discord.RichEmbed();
    rich.setTitle(title);
    rich.setColor(6605242);
    rich.setDescription(text);
    //TODO send random gif in footer, not just baldi taking the L
    rich.setFooter('With ❤, EmojiBot', 'https://cdn.discordapp.com/emojis/528783104643694593.gif');
    return rich;
}

client.on('ready', () =>
{
    console.log('Fetching messages...');
    // Prefetch messages so the bot can add emojis to messages sent before it was turned on
    client.guilds.forEach((guild, id) =>
    {
        guild.channels.forEach((channel, id) =>
        {
            if(channel.type === 'text')
            {
                channel.fetchMessages();
            }
        });
    });
    console.log('Client is ready!');
});

client.on('messageReactionAdd', (messageReaction, user) =>
{
    // If the added emoji was the custom one, remove it and send the tutorial / message to the user's dms
    if(messageReaction.emoji.name === settings['custom-emoji-name'])
    {
        messageReaction.remove(user);
        userWaiting[user.id] = messageReaction.message;
    
        user.send(createMessage('Tutorial', '**1.** Find the emoji id you want from ' + settings['list-url'] + '\n' +
            '**2.** Then, send the plain id here and I\'ll check it off and react to the selected message with that emoji.\n' +
            '**3.** Finally, go to your selected message and click on the emoji to also react to it. (if you don\'t, it will disappear after 5m)'));
        
        let messageRich = new Discord.RichEmbed();
        messageRich.setTitle('Reacting To:');
        // Trim the text if it's beyond 50 chars
        let desc = '"';
        if(messageReaction.message.cleanContent.length > 50)
        {
            desc += messageReaction.message.cleanContent.substring(0, 50) + '..."';
        }
        else
        {
            desc += messageReaction.message.cleanContent + '"';
        }
        messageRich.setDescription(desc);
        messageRich.setTimestamp(messageReaction.message.createdTimestamp);
        messageRich.setFooter(user.username, user.avatarURL);
        user.send(messageRich);
    }
    else if(messageReaction.me)
    {
        // If someone seconded the reaction, remove it
        for(let waiting of emojiWaiting)
        {
            if(messageReaction.message.id === waiting.message.id &&
               messageReaction.emoji.id === waiting.emojiID &&
               user.id === waiting.userID)
            {
                clearTimeout(waiting.timeoutID);
                emojiWaiting.splice(emojiWaiting.indexOf(waiting), 1);
                messageReaction.remove();
                break;
            }
        }
    }
});

client.on('message', (message) =>
{
    // Can't be from itself
    if(message.author.id === client.user.id)
    {
        return;
    }
    
    let isDM = (message.channel.type === 'dm');
    
    // Require a "!" prefix when not in dms
    let prefix = isDM ? '' : settings['prefix'];
    
    if(message.content === prefix + 'list')
    {
        message.channel.send(createMessage('Emoji List', settings['list-url']));
        return;
    }
    
    if(message.content === prefix + 'help')
    {
        //TODO add custom reaction emoji
        message.channel.send(createMessage('Commands', prefix + 'help - Show help\n' + prefix + 'list - List emoji\n' +
            'React with custom then follow the instructions in your dms'));
        return;
    }
    
    // The bot will only accept emoji ids in dms
    if(isDM)
    {
        // User is reacting to something
        if(!userWaiting.hasOwnProperty(message.author.id))
        {
            message.channel.send(createMessage('Not Reacting to Anything', 'Please use the custom emoji on a message to begin reacting'));
            return;
        }
        
        let emoji = emojis.getEmoji(message.content);
        if(emoji === null)
        {
            message.channel.send(createMessage('Invalid ID', 'Please use "' + prefix + 'list" for valid ids'));
            return;
        }
        
        // Valid emoji id, add it
        let selectedMessage = userWaiting[message.author.id];
        selectedMessage.react(emoji.id);
    
        // After the delay (if nobody seconded), remove the reaction
        let timeID = setTimeout(() =>
        {
            for(let reaction of selectedMessage.reactions.array())
            {
                if(reaction.emoji.id === emoji.id && reaction.me)
                {
                    reaction.remove();
                }
            }
            
            for(let wait of emojiWaiting)
            {
                if(wait.message.id === selectedMessage.id && wait.emojiID === emoji.id)
                {
                    emojiWaiting.splice(emojiWaiting.indexOf(wait), 1);
                }
            }
            console.log('Removed old reaction');
        }, settings['timeout']);
        
        // Add it to the waiting queue so if a user seconds it, it gets removed
        emojiWaiting.push({
            'message': selectedMessage,
            'emojiID': emoji.id,
            'timeoutID': timeID,
            'userID': message.author.id
        });
        message.react('✅');
    }
});

client.on('error', (error) =>
{
   console.log('Error: ' + error);
});

console.log('Logging in...');
client.login(settings['secret']);